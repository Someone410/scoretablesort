﻿#include <iostream>  
#include <algorithm> 
using namespace std;

class player
{
	string name;
	int points = 0;

public:

	friend bool sorter(const player&, const player&);
	
	string get_name()const { return name; }
	int get_points()const { return points; }
	
	void set_name(string n) { name = n; }
	void set_points(int n) { points = n; }
};

bool sorter(const player& l, const player& r)
{
	return l.points > r.points;
}

int main() {
	
	system("chcp 1251 > nul");
	
	int n;
	
	cout << "Введите число игроков: ";
	cin >> n;
	
	player* ScoreTable = new player[n];
	
	for (int i = 0; i < n; i++)
	{
		string name;
		int point;
		
		cout << '\n' << "Введите данные игрока " << i+1 << " - сначала имя, затем очки (через пробел либо Enter): " << '\n';
		cin >> name >> point;
		
		ScoreTable[i].set_name(name);
		ScoreTable[i].set_points(point);
	}
	
	sort(ScoreTable, ScoreTable + n, sorter);
	
	for (int i = 0; i < n; i++)
	{
		cout << '\n' << ScoreTable[i].get_name() << " " << ScoreTable[i].get_points();
	}
	
	delete[] ScoreTable;
}